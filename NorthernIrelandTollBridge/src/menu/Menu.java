package menu;

import java.util.Scanner;

/**
 * Object class to create new command line menus for use with programs.
 * 
 * @version 1.2
 * @since 10/12/2019
 * @author ori16108422
 */
public class Menu {

    private String title;
    private String[] optionList;

    /**
     *
     * @param title - String
     * @param options - String[]
     */
    public Menu(String title, String[] options) {
        this.title = title;
        optionList = options;
    }//Menu

    /**
     *
     * @return String
     */
    private String displayMenu() {
        int numOfOptions;
        numOfOptions = optionList.length;

        StringBuilder sb = new StringBuilder();
        sb.append("\n").append(title).append("\n\n");

        for (int index = 0; index < numOfOptions; index++) {
            sb.append(index + 1);
            sb.append(". ").append(optionList[index]).append("\n");
        }//for

        sb.append("\nYour choice? : ");

        return sb.toString();
    }//displayMenu
    
    /**
     * 
     * @return int
     */
    public int getChoice(){
        Scanner kb = new Scanner(System.in);
        System.out.print(displayMenu());
        int choice = kb.nextInt();
        //kb.next();
        
        return choice;
    }//getChoice

}//class
