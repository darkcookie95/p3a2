/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvmsystem;

import java.text.DecimalFormat;

/**
 * Object class to create new VehicleNodes for use with a dynamic queue system.
 * Provides functionality to set next and previous nodes in queue and return
 * values for all fields.
 *
 * @version 1.1
 * @serial 28/11/2019
 * @author ori16108422
 *
 */
public class VehicleNode {

    private String regNum, type, nationality; //Stores registration num, vehicle type and nationailty
    private int passengers; //Stores number of passengers in vehicle
    private double charge, tonnage; //Stores charge value and tonnage of vehicle
    private VehicleNode next, previous; //Stores VehicleNodes that are in front and behind this vehicle

    /**
     * Constructor to instantiate new VehicleNode with passed parameters. charge
     * value is set depending on vehicle type passed.
     *
     * @param regNum - String
     * @param type - String
     * @param nationality - String
     * @param passengers - int
     * @param tonnage - double
     */
    public VehicleNode(String regNum, String type, String nationality,
            int passengers, double tonnage) {
        this.regNum = regNum;
        this.type = type;
        this.nationality = nationality;
        this.passengers = passengers;
        this.tonnage = tonnage;

        //Initiallises the charge value based on what the vehicle type is.
        switch (type.toUpperCase()) {
            case "CAR":
                charge = 1.0;
                break;
            case "MINI-BUS":
                charge = 4.0;
                break;
            default:
                charge = 6.0;
                break;
        }//switch

        next = null;
        previous = null;
    }//VehicleNode

    /**
     * Method to get VehicleNode that is in front of this vehicle in queue.
     *
     * @return VehicleNode
     */
    public VehicleNode getNext() {
        return next;
    }//getNext

    /**
     * Method to VehicleNode that is behind this vehicle in queue.
     *
     * @return VehicleNode
     */
    public VehicleNode getPrevious() {
        return previous;
    }//getPrevious

    /**
     * Method to get registration number of vehicle.
     *
     * @return String
     */
    public String getRegNum() {
        return regNum;
    }

    /**
     * Method to get the type of vehicle.
     *
     * @return String
     */
    public String getType() {
        return type;
    }//getType

    /**
     * Method to get nationality.
     *
     * @return String
     */
    public String getNationality() {
        return nationality;
    }//getNationality

    /**
     * Method to get number of passengers.
     *
     * @return int
     */
    public int getPassengers() {
        return passengers;
    }//getPassengers

    /**
     * Method to get charge value.
     *
     * @return double
     */
    public double getCharge() {
        return charge;
    }//getCharge

    /**
     * Method to get tonnage.
     *
     * @return double
     */
    public double getTonnage() {
        return tonnage;
    }//getTonnage

    /**
     * Method to set VehicleNode that is in front of this vehicle in queue.
     *
     * @param next - VehicleNode
     */
    public void setNext(VehicleNode next) {
        this.next = next;
    }//setNext

    /**
     * Method to set VehicleNode that is behind this vehicle in queue.
     *
     * @param previous - VehicleNode
     */
    public void setPrevious(VehicleNode previous) {
        this.previous = previous;
    }//setPrevious

    /**
     * Method to get all details about vehicle.
     *
     * @return String
     */
    public String details() {
        StringBuilder sb = new StringBuilder();
        DecimalFormat df = new DecimalFormat("0.00");

        sb.append(regNum).append("\t\t")
                .append(type).append("\t\t")
                .append("£").append(df.format(charge)).append("\t\t")
                .append(passengers).append("\t\t")
                .append(tonnage).append("\t\t")
                .append(nationality).append("\n");

        return sb.toString();
    }//detils

}//class END
