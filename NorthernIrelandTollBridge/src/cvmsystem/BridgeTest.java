/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvmsystem;

import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Prototype CMD Line system to be used with CVMQueue and VehicleNode object
 * classes. Allows for the adding and removing of vehicle from the queue,
 * display details about the queue and details about vehicles in the queue.
 *
 * @author ori16108422
 * @version 1.1
 * @since 08/12/2019
 */
public class BridgeTest {

    //Instantiates a new CVMQueue for use with the program.
    private static CVMQueue myQ = new CVMQueue(15.5);

    public static void main(String[] args) {

        String[] options = {"Add vehicle to queue", "Remove vehicle from queue",
            "List all vehicle details in queue", "Display total charge", "Display total weight",
            "Display specific vehicle details", "Display total passengers",
            "Display average weight", "Tally nationalities", "Display total vehicles crossed",
            "Exit"};

        //Instantiates a menu object using the options array.
        menu.Menu myMenu = new menu.Menu("CVM System", options);

        int choice = 0;
        while (choice != 11) {
            try {
                choice = myMenu.getChoice();
            }//try
            catch (InputMismatchException ex) {
                System.out.println("Error: Please enter a number");
            }
            menuSwitch(choice);
        }//while
    }//main

    /**
     * Method passing user input for menu choice and executing a switch
     * statement to execute relevant functionality.
     *
     * @param choice - int
     */
    private static void menuSwitch(int choice) {
        Scanner kbNum = new Scanner(System.in);
        Scanner kbStr = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("0.00");

        switch (choice) {
            case 1:
                System.out.println("Add Vehicle to Queue");
                System.out.println("********************");

                String reg = "";
                //While loop to continuosly ask for a reg# if user tries to 
                //continue without entering anything.
                while (reg.equals("")) {
                    System.out.print("\nEnter vehicle reg #: ");
                    reg = kbStr.nextLine();
                }//while

                //Capitalises all letters in the string
                reg = reg.toUpperCase();

                String type = "";
                //While loop to continuosly ask for vehicle type if user tries to 
                //continue without entering anything.
                while (type.equals("")) {
                    System.out.print("\nEnter vehicle type: ");
                    type = kbStr.nextLine();

                    if (!type.equals("")) {
                        //Capitalises first letter of input.
                        type = type.substring(0, 1).toUpperCase() + type.substring(1);

                        //Verifies that user input for vehicle type is acceptable.
                        //If not, displays and error message and set type to "" in 
                        //order to loop back to start of while loop.
                        if (!(type.equals("Car") || type.equals("Mini-bus")
                                || type.equals("Lorry"))) {
                            System.out.println("Error: Invalid vehicle type. "
                                    + "Please enter Car, Mini-bus or Lorry");
                            type = "";
                        }//if
                    }//if
                }//while

                int passengers = 0;
                while (passengers <= 0) {
                    System.out.print("\nEnter number of passengers: ");

                    try {
                        passengers = kbNum.nextInt();
                    }//try
                    catch (InputMismatchException ex) {
                        System.out.println("Error: Please enter a number");
                        passengers = 0;
                    }//catch

                    //Verifies that user input for passengers is above 0.
                    //If not, displays and error message and loops back to start of while loop.
                    if (passengers <= 0) {
                        System.out.println("Error: Please enter a value above 0");
                    }//if
                }//while

                double tonnage = 0;
                while (tonnage <= 0) {
                    System.out.print("\nEnter vehicle tonnage: ");

                    try {
                        tonnage = kbNum.nextDouble();
                    }//try
                    catch (InputMismatchException ex) {
                        System.out.println("Error: Please enter a number");
                        tonnage = 0;
                    }//while

                    //Verifies that user input for tonnage is above 0.
                    //If not, displays and error message and loops back to start of while loop.
                    if (tonnage <= 0) {
                        System.out.println("Error: Please enter a value above 0");
                    }//if
                }//while

                String nationality = "";
                //While loop to continuosly ask for driver nationality if user 
                //tries to continue without entering anything.
                while (nationality.equals("")) {
                    System.out.print("\nEnter driver nationality: ");
                    nationality = kbStr.nextLine();
                }//while
                
                //Capitalises first letter of input.
                nationality = nationality.substring(0, 1).toUpperCase() + nationality.substring(1);

                //Instantiates and enqueues a new vehicle node.
                myQ.enqueue(new VehicleNode(reg, type, nationality, passengers, tonnage));

                break;
            case 2:
                System.out.println("Remove Vehicle from Queue");
                System.out.println("*************************");

                //Checks to see if there is a vehicle in the queue. 
                //If not, states that the queue is empty, otherwise displays 
                //vehicle reg# that has left queue.
                if (myQ.peek() == null) {
                    System.out.println("\nQueue is empty. No vehicle to remove.");
                }//if
                else {
                    System.out.println("\n" + myQ.dequeue().getRegNum() + " left the queue");
                }//else
                break;
            case 3:
                System.out.println("List Details of All Vehicles in Queue");
                System.out.println("*************************************");

                //Calls CVMQueue's list method to list all vehicle details in queue.
                System.out.println("\n" + myQ.list());

                break;
            case 4:
                System.out.println("Total Charge of Vehicles in Queue");
                System.out.println("*********************************");

                //Displays totalCharge to 2dp.
                System.out.println("\n£" + df.format(myQ.getTotalCharge()));

                break;
            case 5:
                System.out.println("Total Weight of Vehicles in Queue");
                System.out.println("*********************************");

                //Displays totalTonnage to 2dp.
                System.out.println("\n" + df.format(myQ.getTotalTonnage()) + " tonnes");

                break;
            case 6:
                System.out.println("Details of Specific Vehicle in Queue");
                System.out.println("************************************");

                //Checks that there are vehicles currently in the queue.
                if (myQ.getSize() != 0) {
                    //Displays current number of vehicles in queue.
                    System.out.println("\nThere are currently " + myQ.getSize() + " vehicles in queue");
                    System.out.print("\nEnter queue position of vehicle wanted: ");
                    int position = -1;

                    try {
                        position = kbNum.nextInt();
                    }//try
                    catch (InputMismatchException ex) {
                        System.out.println("Error: Please enter a number");
                    }//catch

                    //Displays returned details for vehicle found at user entered position.
                    System.out.println(myQ.findVehicle(position));
                }//if
                else {
                    System.out.println("\nThe queue is empty");
                }//else

                break;
            case 7:
                System.out.println("Total Number of Passengers in Queue");
                System.out.println("***********************************");

                //Displays totalPassengers
                System.out.println("\n" + myQ.getTotalPassengers());

                break;
            case 8:
                System.out.println("Average Weight of Vehicles in Queue");
                System.out.println("***********************************");

                //Displays avgTonnage to 2dp
                System.out.println("\n" + df.format(myQ.getAvgTonnage()));

                break;
            case 9:
                System.out.println("Tally of Driver Nationalities in Queue");
                System.out.println("**************************************");

                //Displays nationality tally
                System.out.println(myQ.tallyNationality());

                break;
            case 10:
                System.out.println("Total Number of Vehicles Crossed");
                System.out.println("********************************");

                //Displays number of vehicles crossed in CVMQueue's lifetime.
                System.out.println("\n" + myQ.getTotalVehiclesCrossed());

                break;
            case 11:
                System.out.println("Thank you for using the NITB CVM system.");
                System.out.println("Exiting system...");

                break;
            default:
                System.out.println("Error: Please enter a number from the list of options");
                break;
        }//switch
    }//menuSwitch

}//class END
