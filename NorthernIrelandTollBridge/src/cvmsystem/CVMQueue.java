/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvmsystem;

/**
 * Object class to create dynamic Crossing Vehicle Management (CVM) queue.
 * Provides functionality to nodes at start and end of queue, list all nodes
 * currently in queue, total values for charge, tonnage and passengers, average
 * tonnage currently in queue and usage statistics.
 *
 * @author ori16108422
 * @version 1.0
 * @since 26/11/2019
 */
public class CVMQueue {

    private VehicleNode start, end; //Stores nodes at start and end of queue
    private double totalCharge, totalTonnage, avgTonnage; //Stores current total charge, tonnage and average tonnage
    private int totalPassengers, totalVehiclesCrossed, size; //Stores current total passengers, size of queue and total lifetime vehicles crossed
    private double maxTonnage; //Stores the max tonnage the queue can hold at any time

    /**
     * Constructor to instantiate new dynamic CVMQueue.
     *
     * @param maxTonnage - double
     */
    public CVMQueue(double maxTonnage) {
        this.maxTonnage = maxTonnage;
        start = null;
        end = null;
    }//CVMQueue

    /**
     * Method to check if queue is currently empty.
     *
     * @return Boolean
     */
    private boolean isEmpty() {
        return (start == null);
    }//isEmpty

    /**
     * Method to calculate average tonnage currently in queue.
     */
    private void calcAvgTonnage() {
        if (size == 0) {
            avgTonnage = 0.0;
        }//if
        else {
            avgTonnage = totalTonnage / size;
        }//else
    }//calcAvgTonnage

    /**
     * Method to check if a VehicleNode or a VehicleNode with an identical Reg#,
     * is already in the queue.
     *
     * @param nd - VehicleNode
     * @return Boolean
     */
    private boolean contains(VehicleNode nd) {
        if (isEmpty()) {
            return false;
        }//if

        VehicleNode ref = start;

        while (ref != null) {
            if (ref.equals(nd)) {
                return true;
            }//if
            else if (ref.getRegNum().equals(nd.getRegNum())) {
                return true;
            }//else if

            ref = ref.getPrevious();
        }//while

        return false;
    }//contains

    /**
     * Method to add new node into the queue. Only allows node to be added if
     * totalTonnage will not go over maxTonnage. Increases totalCharge,
     * totalTonnage, totalPassengers and size. Calls calcAvgTonnage().
     *
     * @param nd - VehicleNode
     */
    public void enqueue(VehicleNode nd) {
        //Checks if the node is already in the queue and only continues if it isn't.
        if (!(contains(nd))) {
            //Will only add node to queue is its tonnage + totalTonnage not greater than the max tonnage.
            if (!((totalTonnage + nd.getTonnage()) > maxTonnage)) {
                if (isEmpty()) {
                    start = nd;
                    end = nd;
                }//if
                else {
                    nd.setNext(end);
                    end.setPrevious(nd);
                    end = nd;
                }//else

                size++;
                totalCharge += nd.getCharge();
                totalTonnage += nd.getTonnage();
                totalPassengers += nd.getPassengers();
                calcAvgTonnage();
            }//if

        }//if
    }//enqueue

    /**
     * Method to remove node from the queue. Decreases totalCharge,
     * totalTonnage, totalPassengers and size. Calls calcAvgTonnage().
     *
     * @return VehicleNode
     */
    public VehicleNode dequeue() {
        if (isEmpty()) {
            return null;
        }//if

        VehicleNode removed = start;

        if (start == end) {
            start = null;
            end = null;
        }//if
        else {
            start = start.getPrevious();
            start.setNext(null);
        }//else

        size--;
        totalVehiclesCrossed++;
        totalCharge -= removed.getCharge();
        totalTonnage -= removed.getTonnage();
        totalPassengers -= removed.getPassengers();
        calcAvgTonnage();

        return removed;
    }//dequeue

    /**
     * Method to display list of details for all vehicles currently in queue.
     *
     * @return String
     */
    public String list() {
        StringBuilder sb = new StringBuilder();

        if (isEmpty()) {
            sb.append("Queue is empty");
            return sb.toString();
        }//if

        VehicleNode current = start;
        sb.append("Reg #\t\tType\t\tCharge\t\tPassengers\tTonnage\t\tNationality\n");
        sb.append("*****\t\t****\t\t******\t\t***********\t*******\t\t**********\n");

        do {
            sb.append(current.details());

            current = current.getPrevious();

        }//do
        while (current != null);

        return sb.toString();
    }//list

    /**
     * Method to get the VehicleNode at the start of the queue.
     *
     * @return - VehicleNode
     */
    public VehicleNode peek() {
        return start;
    }//peak

    /**
     * Method to get the VehicleNode at the end of the queue.
     *
     * @return - VehicleNode
     */
    public VehicleNode getEnd() {
        return end;
    }//getEnd

    /**
     * Method to get total charge value currently in queue.
     *
     * @return double
     */
    public double getTotalCharge() {
        return totalCharge;
    }//getTotalCharge

    /**
     * Method to get total tonnage currently in queue.
     *
     * @return double
     */
    public double getTotalTonnage() {
        return totalTonnage;
    }//getTotalTonnage

    /**
     * Method to get average tonnage currently in queue.
     *
     * @return double
     */
    public double getAvgTonnage() {
        return avgTonnage;
    }//getAvgTonnage

    /**
     * Method to get total number of passengers currently in queue.
     *
     * @return int
     */
    public int getTotalPassengers() {
        return totalPassengers;
    }//getTotalPassengers

    /**
     * Method to get total number of vehicles that have crossed the queue in its
     * lifetime.
     *
     * @return int
     */
    public int getTotalVehiclesCrossed() {
        return totalVehiclesCrossed;
    }//getTotalVehiclesCrossed

    /**
     * Method to get the current size of queue.
     *
     * @return int
     */
    public int getSize() {
        return size;
    }//getSize

    /**
     * Method to get the max weight allowance of the queue
     *
     * @return double
     */
    public double getMaxTonnage() {
        return maxTonnage;
    }//getMaxTonnage

    /**
     * Method to get details about VehicleNode at specific position in the
     * queue.
     *
     * @param qPos - int
     * @return String
     */
    public String findVehicle(int qPos) {
        if (qPos > size || qPos <= 0) {
            return "No vehicle at that position";
        }//if
        VehicleNode current = start;
        for (int index = 1; index < qPos; index++) {
            current = current.getPrevious();
        }//for

        StringBuilder sb = new StringBuilder();

        sb.append("Reg #\t\tType\t\tCharge\t\tPassengers\tTonnage\t\tNationality\n");
        sb.append("*****\t\t****\t\t******\t\t***********\t*******\t\t**********\n");
        sb.append(current.details());

        return sb.toString();
    }//findVehicle

    /**
     * Method to tally all nationalities currently in queue.
     *
     * @return String
     */
    public String tallyNationality() {
        if (isEmpty()) {
            return "Queue is empty";
        }//if

        StringBuilder sb = new StringBuilder();

        sb.append("Nationality\tAmount\n");
        sb.append("***********\t******\n");

        VehicleNode current = start;

        for (int index = 0; index < size; index++) {
            String nationality = current.getNationality();

            //This checks if the value of nationality has already been added to the StringBuilder. 
            //If it has, this section is skipped.
            if (!(sb.indexOf(nationality) > -1)) {
                sb.append(nationality).append("\t\t");
                int tally = 0;
                VehicleNode check = current;

                for (int index2 = index; index2 < size; index2++) {
                    if (nationality.equals(check.getNationality())) {
                        tally++;
                    }//if
                    check = check.getPrevious();
                }//for

                sb.append(tally).append("\n");
            }//if

            current = current.getPrevious();
        }//for

        return sb.toString();
    }//tallyNationality

}//class END
