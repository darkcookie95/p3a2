/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvmsystem;

import org.junit.*;
import static org.junit.Assert.*;
import org.junit.runners.MethodSorters;

/**
 * JUnit test class for CVMQueue.
 * @author ori16108422
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //Forces the unit tests to run in alphabetical order
public class CVMQueueTest {

    //static variables to be used in unit tests
    private static CVMQueue testQ;
    private static VehicleNode vNode1;
    private static VehicleNode vNode2;
    private static VehicleNode vNode3;
    private static VehicleNode vNode4;
    private static VehicleNode vNode5;

    /**
     * Method that runs once, before all unit tests to initialise static object
     * variables instead of initialising them in every test.
     */
    @BeforeClass
    public static void setUpClass() {
        testQ = new CVMQueue(8.5);
        vNode1 = new VehicleNode("OXB 123", "Car", "British", 3, 1.2);
        vNode2 = new VehicleNode("BR 22", "Mini-bus", "Italian", 10, 2.2);
        vNode3 = new VehicleNode("5FR 789", "Car", "British", 2, 1.5);
        vNode4 = new VehicleNode("PL 34BR", "Lorry", "French", 1, 4.4);
        vNode5 = new VehicleNode("OXB 123", "Car", "British", 3, 1.2);
    }//setUpClass

    /**
     * Test of CVMQueue constructor method. Will pass if class has been
     * instantiated.
     */
    @Test
    public void test000Constructor() {
        System.out.println("CVMQueue constructor");
        int expResult = 0;
        int result = testQ.getTotalVehiclesCrossed();

        assertEquals(result, expResult);
    }//test000Constructor

    /**
     * Test of enqueue method while the queue is empty.
     */
    @Test
    public void test001EnqueueWithEmptyQueue() {
        System.out.println("enqueue - with empty queue");
        testQ.enqueue(vNode1);
        VehicleNode result = testQ.getEnd();

        assertEquals(vNode1, result);
    }//test001EnqueueWithEmptyQueue

    /**
     * Test of enqueue method while there are multiple nodes in the queue.
     */
    @Test
    public void test002EnqueueWithPopulatedQueue() {
        System.out.println("enqueue - with populated queue");
        testQ.enqueue(vNode2);
        testQ.enqueue(vNode3);
        VehicleNode result = testQ.getEnd();

        assertEquals(vNode3, result);
    }//test002EnqueueWithPopulatedQueue

    /**
     * Test of enqueue method, attempting to enqueue a node that is already in
     * the queue.
     */
    @Test
    public void test003EnqueueSameNodeTwice() {
        System.out.println("enqueue - same node twice");
        testQ.enqueue(vNode1);
        VehicleNode result = testQ.getEnd();

        assertEquals(vNode3, result);
    }//test003EnqueueSameNodeTwice

    /**
     * Test of enqueue method, attempting to enqueue a new node with a matching
     * reg# to another node (e.g. is a duplicate vehicle).
     */
    @Test
    public void test004EnqueueDuplicateVehicle() {
        System.out.println("enqueue - duplicate vehicle");
        testQ.enqueue(vNode5);
        VehicleNode result = testQ.getEnd();

        assertEquals(vNode3, result);
    }//test004EnqueueDuplicateVehicle

    /**
     * Test of enqueue method, where the node added will put the queue above its
     * max weight limit.
     */
    @Test
    public void test005EnqueueVehicleThatPutsQueueOverMaxTonnage() {
        System.out.println("enqueue - vehicle that puts queue over max tonnage");
        testQ.enqueue(vNode4);
        VehicleNode result = testQ.getEnd();

        assertEquals(vNode3, result);
    }//test005EnqueueVehicleThatPutsQueueOverMaxTonnage

    /**
     * Test of dequeue method, while there are multiple node in the queue.
     */
    @Test
    public void test006DequeuePopulatedQueue() {
        System.out.println("dequeue - with populated queue");
        VehicleNode result = testQ.dequeue();

        assertEquals(vNode1, result);
    }//test006DequeuePopulatedQueue

    /**
     * Test of list method, while there are nodes in the queue.
     */
    @Test
    public void test007ListPopulatedQueue() {
        System.out.println("list - with populated queue");

        StringBuilder sb = new StringBuilder();
        sb.append("Reg #\t\tType\t\tCharge\t\tPassengers\tTonnage\t\tNationality\n");
        sb.append("*****\t\t****\t\t******\t\t***********\t*******\t\t**********\n");
        sb.append("BR 22").append("\t\t")
                .append("Mini-bus").append("\t\t")
                .append("£4.00").append("\t\t")
                .append("10").append("\t\t")
                .append("2.2").append("\t\t")
                .append("Italian").append("\n");
        sb.append("5FR 789").append("\t\t")
                .append("Car").append("\t\t")
                .append("£1.00").append("\t\t")
                .append("2").append("\t\t")
                .append("1.5").append("\t\t")
                .append("British").append("\n");

        String expResult = sb.toString();
        String result = testQ.list();

        assertEquals(expResult, result);
    }//test007ListPopulatedQueue

    /**
     * Test of getTotalCharge method.
     */
    @Test
    public void test008GetTotalCharge() {
        System.out.println("getTotalCharge");
        double expResult = 5.0;
        double result = testQ.getTotalCharge();

        assertEquals(expResult, result, 0.0);
    }//test008GetTotalCharge

    /**
     * Test of getTotalTonnage method.
     */
    @Test
    public void test009GetTotalTonnage() {
        System.out.println("getTotalTonnage");
        double expResult = 3.7;
        double result = testQ.getTotalTonnage();
        assertEquals(expResult, result, 0.0);
    }//test009GetTotalTonnage

    /**
     * Test of getAvgTonnage method, while there are nodes in the queue.
     */
    @Test
    public void test010GetAvgTonnagePopulatedQueue() {
        System.out.println("getAvgTonnage - with populated queue");
        double expResult = 1.85;
        double result = testQ.getAvgTonnage();
        assertEquals(expResult, result, 0.00001);
    }//test010GetAvgTonnagePopulatedQueue

    /**
     * Test of getTotalPassengers method.
     */
    @Test
    public void test011GetTotalPassengers() {
        System.out.println("getTotalPassengers");
        int expResult = 12;
        int result = testQ.getTotalPassengers();
        assertEquals(expResult, result);
    }//test011GetTotalPassengers

    /**
     * Test of findVehicle method, while there are nodes in the queue and
     * searched position is valid.
     */
    @Test
    public void test012FindVehiclePopulatedQueueValidPosition() {
        System.out.println("findVehicle - with populated queue and valid queue position");
        int qPos = 2;

        StringBuilder sb = new StringBuilder();
        sb.append("Reg #\t\tType\t\tCharge\t\tPassengers\tTonnage\t\tNationality\n");
        sb.append("*****\t\t****\t\t******\t\t***********\t*******\t\t**********\n");
        sb.append("5FR 789").append("\t\t")
                .append("Car").append("\t\t")
                .append("£1.00").append("\t\t")
                .append("2").append("\t\t")
                .append("1.5").append("\t\t")
                .append("British").append("\n");

        String expResult = sb.toString();
        String result = testQ.findVehicle(qPos);
        assertEquals(expResult, result);
    }//test012FindVehiclePopulatedQueueValidPosition

    /**
     * Test of findVehicle method, while there are nodes in the queue but the
     * searched position is invalid.
     */
    @Test
    public void test013FindVehiclePopulatedQueueInvalidPosition() {
        System.out.println("findVehicle - with populated queue and invalid queue position");
        int qPos = 0;
        String expResult = "No vehicle at that position";
        String result = testQ.findVehicle(qPos);
        assertEquals(expResult, result);
    }//test013FindVehiclePopulatedQueueInvalidPosition

    /**
     * Test of tallyNationality method, while there are nodes in the queue.
     */
    @Test
    public void test014TallyNationalityPopulatedQueue() {
        System.out.println("tallyNationality - with populated queue");

        StringBuilder sb = new StringBuilder();
        sb.append("Nationality\tAmount\n");
        sb.append("***********\t******\n");
        sb.append("Italian\t\t1\n");
        sb.append("British\t\t1\n");

        String expResult = sb.toString();
        String result = testQ.tallyNationality();
        assertEquals(expResult, result);
    }//test014TallyNationalityPopulatedQueue

    /**
     * Test of dequeue method, with no nodes in the queue.
     */
    @Test
    public void test015DequeueEmptyQueue() {
        System.out.println("dequeue - with empty queue");
        testQ.dequeue();
        testQ.dequeue();
        VehicleNode result = testQ.dequeue();
        assertEquals(null, result);
    }//test015DequeueEmptyQueue

    /**
     * Test of list method, with no nodes in the queue.
     */
    @Test
    public void test016ListEmptyQueue() {
        System.out.println("list - with empty queue");
        String expResult = "Queue is empty";
        String result = testQ.list();
        assertEquals(expResult, result);
    }//test016ListEmptyQueue

    /**
     * Test of peek method.
     */
    @Test
    public void test017Peek() {
        System.out.println("peek");
        VehicleNode result = testQ.peek();
        assertEquals(null, result);
    }//peek

    /**
     * Test of getEnd method.
     */
    @Test
    public void test018GetEnd() {
        System.out.println("getEnd");
        VehicleNode result = testQ.getEnd();
        assertEquals(null, result);
    }//test018GetEnd

    /**
     * Test of getAvgTonnage method, with no nodes in the queue.
     */
    @Test
    public void test019GetAvgTonnageEmptyQueue() {
        System.out.println("getAvgTonnage - with empty queue");
        double expResult = 0.0;
        double result = testQ.getAvgTonnage();
        assertEquals(expResult, result, 0.0);
    }//test019GetAvgTonnageEmptyQueue

    /**
     * Test of tallyNationality method, with no nodes in the queue.
     */
    @Test
    public void test020TallyNationalityEmptyQueue() {
        System.out.println("tallyNationality - with empty queue");
        String expResult = "Queue is empty";
        String result = testQ.tallyNationality();
        assertEquals(expResult, result);
    }//test020TallyNationalityEmptyQueue

    /**
     * Test of getTotalVehiclesCrossed method.
     */
    @Test
    public void test021GetTotalVehiclesCrossed() {
        System.out.println("getTotalVehiclesCrossed");
        int expResult = 3;
        int result = testQ.getTotalVehiclesCrossed();
        assertEquals(expResult, result);
    }//test021GetTotalVehiclesCrossed

    /**
     * Test of getSize method.
     */
    @Test
    public void test022GetSize() {
        System.out.println("getSize");
        int expResult = 0;
        int result = testQ.getSize();
        assertEquals(expResult, result);
    }//test022GetSize

    /**
     * Test of getMaxTonnage method.
     */
    @Test
    public void test023GetMaxTonnage() {
        System.out.println("getMaxTonnage");
        double expResult = 8.5;
        double result = testQ.getMaxTonnage();
        assertEquals(expResult, result, 0.0);
    }//test023GetMaxTonnage

}//test class END
